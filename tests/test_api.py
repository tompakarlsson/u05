"""Unit tests for API"""
import datetime
from uuid import UUID


def test_root_ok(fast_api_client):
    """
    Test if root returns correct welcome message
    """
    response = fast_api_client.get("/")
    assert response.status_code == 200
    assert response.json() == {"greeting": "Hello visitor!"}


def test_stores_ok(mocker, fast_api_client):
    """
    Test to assert if endpoint returns correct status code and data
    """
    mocker.patch("u05.app.main.app.db.cursor.fetchall", return_value=[
        ('Djurjouren', 'Stockholm', 'Upplandsgatan 99', '12345'),
        ('Djuristen', 'Falun', 'Skånegatan 420', '54321'),
        ('Den Lilla Djurbutiken', 'Hudiksvall', 'Nätverksgatan 22', '55555'),
        ('Den Stora Djurbutiken', 'Hudiksvall', 'Routergatan 443', '54545'),
        ('Noahs Djur & Båtaffär', 'Gävle', 'Stallmansgatan 666', '96427')
    ])
    response = fast_api_client.get("/stores")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {
                "name": "Djurjouren",
                "address": "Upplandsgatan 99, 12345 Stockholm"
            },
            {
                "name": "Djuristen",
                "address": "Skånegatan 420, 54321 Falun"
            },
            {
                "name": "Den Lilla Djurbutiken",
                "address": "Nätverksgatan 22, 55555 Hudiksvall"
            },
            {
                "name": "Den Stora Djurbutiken",
                "address": "Routergatan 443, 54545 Hudiksvall"
            },
            {
                "name": "Noahs Djur & Båtaffär",
                "address": "Stallmansgatan 666, 96427 Gävle"
            }
        ]
    }


def test_stores_by_name_ok(mocker, fast_api_client):
    """
    Test to assert if a correct store name returns correct status code and data
    """
    mocker.patch("u05.app.main.app.db.cursor.fetchall", return_value=[
        ('Djuristen', 'Falun', 'Skånegatan 420', '54321')])
    response = fast_api_client.get("/stores/Djuristen")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {
                "name": "Djuristen",
                "address": "Skånegatan 420, 54321 Falun"
            }
        ]
    }


def test_cities_ok(mocker, fast_api_client):
    """
    Test to assert if endpoint returns correct status code and data
    """
    mocker.patch("u05.app.main.app.db.cursor.fetchall", return_value=[
        ('Falun',), ('Gävle',), ('Stockholm',), ('Hudiksvall',)])
    response = fast_api_client.get("/cities")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            "Falun",
            "Gävle",
            "Stockholm",
            "Hudiksvall"
        ]
    }


def test_cities_zip_ok(mocker, fast_api_client):
    """
    Test to assert if a correct zip code returns correct status code and data
    """
    mocker.patch("u05.app.main.app.db.cursor.fetchall", return_value=[('Stockholm',)])
    response = fast_api_client.get("/cities?zipcode=12345")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            "Stockholm"
        ]
    }


def test_sales_ok(mocker, fast_api_client):
    """
    Test to assert if return from /sales is correct
    """
    mocker.patch("u05.app.main.app.db.cursor.fetchall", return_value=[
        ('Den Stora Djurbutiken',
         datetime.datetime(2022, 1, 25, 13, 52, 34),
         UUID('0188146f-5360-408b-a7c5-3414077ceb59')),
        ('Djuristen',
         datetime.datetime(2022, 1, 26, 15, 24, 45),
         UUID('726ac398-209d-49df-ab6a-682b7af8abfb')),
        ('Den Lilla Djurbutiken',
         datetime.datetime(2022, 2, 7, 9, 0, 56),
         UUID('602fbf9d-2b4a-4de2-b108-3be3afa372ae')),
        ('Den Stora Djurbutiken',
         datetime.datetime(2022, 2, 27, 12, 32, 46),
         UUID('51071ca1-0179-4e67-8258-89e34b205a1e'))])
    response = fast_api_client.get("/sales")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {
                "store": "Den Stora Djurbutiken",
                "timestamp": "20220125T13:52:34",
                "sale_id": "0188146f-5360-408b-a7c5-3414077ceb59"
            },
            {
                "store": "Djuristen",
                "timestamp": "20220126T15:24:45",
                "sale_id": "726ac398-209d-49df-ab6a-682b7af8abfb"
            },
            {
                "store": "Den Lilla Djurbutiken",
                "timestamp": "20220207T09:00:56",
                "sale_id": "602fbf9d-2b4a-4de2-b108-3be3afa372ae"
            },
            {
                "store": "Den Stora Djurbutiken",
                "timestamp": "20220227T12:32:46",
                "sale_id": "51071ca1-0179-4e67-8258-89e34b205a1e"
            }
        ]
    }


def test_sales_id_by_valid_id(mocker, fast_api_client):
    """
    Tests response from valid id that's also present in database
    """
    mocker.patch("u05.app.main.app.db.cursor.fetchall", return_value=[
        ('Djuristen',
         datetime.datetime(2022, 1, 26, 15, 24, 45),
         UUID('726ac398-209d-49df-ab6a-682b7af8abfb'),
         'Elefantkoppel',
         1)])
    response = fast_api_client.get("/sales/726ac398-209d-49df-ab6a-682b7af8abfb")
    assert response.status_code == 200
    assert response.json() == {
        "data": [
            {
                "store": "Djuristen",
                "timestamp": "20220126T15:24:45",
                "sale_id": "726ac398-209d-49df-ab6a-682b7af8abfb",
                "products": [
                    {
                        "name": "Elefantkoppel",
                        "qty": 1
                    }
                ]
            }
        ]
    }


# Bad tests
def test_stores_by_name_bad(mocker, fast_api_client):
    """
    Test to assert if wrong store name returns correct status code and data
    """
    mocker.patch("u05.app.main.app.db.cursor.fetchall", return_value=[])
    response = fast_api_client.get("/stores/nonexistingstore")
    assert response.status_code == 404
    assert response.json() == {"detail": "404 Not Found"}


def test_cities_zip_bad(mocker, fast_api_client):
    """
    Test to assert if wrong zip code returns correct status code and data
    """
    mocker.patch("u05.app.main.app.db.cursor.fetchall", return_value=[])
    response = fast_api_client.get("/cities?zipcode=99999")
    assert response.status_code == 404
    assert response.json() == {"detail": "404 Not Found"}


def test_sales_id_not_found(mocker, fast_api_client):
    """
    Tests uuid not found in database
    """
    mocker.patch("u05.app.main.app.db.cursor.fetchall", return_value=[])
    response = fast_api_client.get("/sales/19e67404-6e35-45b7-8d6f-e5bc5b79c453")
    assert response.status_code == 404
    assert response.json() == {"detail": "404 Not Found"}


def test_sales_id_by_invalid_id(mocker, fast_api_client):
    """
    Tests uuid by invalid format
    """
    mocker.patch("u05.app.main.app.db.cursor.fetchall", return_value={
        "detail": "422 Unprocessable Entity"})
    response = fast_api_client.get("/sales/invalid-uuid-format")
    assert response.status_code == 422
    assert response.json() == {"detail": "422 Unprocessable Entity"}
